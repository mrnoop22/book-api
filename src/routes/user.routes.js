const express = require('express')
const router = new express.Router()
const userModel = require('../models/user.models')
const auth = require('../middleware/auth')
const adAuth = require('../middleware/adauth')
router.post('/register', async(req,res)=>{
    const body={
        email:req.body.email,
        password:req.body.password,
        confirmpassword:req.body.confirmpassword
    }
    try{
        const user = new userModel(body)
        await user.save()
        res.status(200).send({
            apiStatus:true,
            data:user,
            message:'user added successfully'
        })
    }
    catch(e){
        res.status(500).send({
            apiStatus:false,
            data:e.message,
            message:'invalid operation'
        })
    }
})
router.post('/login',async(req,res)=>{
    try{
        const user = await userModel.loginUser(req.body.email, req.body.password)
        const token = await user.generateAuthToken()
        res.status(200).send({
            apiStatus:true,
            data:{user, token},
            message:"user logged in"
        })
    }
    catch(error){
        res.status(500).send({
            apiStatus: false,
            data: error.message,
            message:'email or password is wrong'
        })
    }
})
router.get('/profile', auth,async(req,res)=>{
    res.send(req.user)
})
router.patch('/profile/update', auth,async(req,res)=>{
    const body={
        username:req.body.username,
        fname:req.body.fname,
        lname:req.body.lname,
        address:req.body.address
    }
    const user = new userModel(body)
    await user.save()
    try{
        await user.save()
        res.status(200).send({
            data:user,
            message:'user updated successfully'
        })
    }
    catch(e){
        res.status(500).send({
            data:e.message,
            message:'invalid operation'
        })
    }
})
router.post('/logout', auth, async(req, res)=>{
    try{
        req.user.tokens = req.user.tokens.filter((element)=>{
            return element!=req.token
        })
        await req.user.save()
        res.status(200).send({
            apiStatus: true,
            data:'',
            message:'logged out'
        })
    }
    catch(error){
        res.status(500).send({
            apiStatus: false,
            data: error.message,
            message:'Please try again'
        })
    }
})
router.get('/activate/:id', async(req, res)=>{
    try{
        const _id= req.params.id
        const user = await userModel.findById({_id})
        if(!user) throw new Error('invalid user id')
        user.accountStatus = true
        await user.save()
        res.status(200).send({
            apiStatus:true,
            data: user,
            message: 'user activated'
        })
    }
    catch(error){
        res.status(500).send({
            apiStatus: false,
            data: error.message,
            message:'Something wrong'
        })
    }
})
router.get('/deactivate',auth, async(req, res)=>{
    try{
        req.user.accountStatus= false
        await user.save()
        res.status(200).send({
            apiStatus:true,
            data: user,
            message: 'user status updated'
        })
    }
    catch(error){
        res.status(500).send({
            apiStatus: false,
            data: error.message,
            message:'user register error'
        })
    }
})
module.exports=router